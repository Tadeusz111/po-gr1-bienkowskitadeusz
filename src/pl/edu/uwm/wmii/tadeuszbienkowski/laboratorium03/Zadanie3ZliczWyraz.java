package pl.edu.uwm.wmii.tadeuszbienkowski.laboratorium03;
import java.io.File;
import java.io.FileNotFoundException;
import java.util.Scanner;

public class Zadanie3ZliczWyraz {
    public static void main(String []args)throws FileNotFoundException{
        Scanner in = new Scanner(System.in);
        System.out.print("podaj sciezkie pliku: ");

        String plik = in.nextLine();
        System.out.print("podaj szukany wyrza: ");
        String wyraz= in.next();
        System.out.println("ilosc wyrazow w pliku wynosi: "+zliczWyrza(plik,wyraz));
    }
    public static int zliczWyrza(String str,String wyraz)throws FileNotFoundException
    {
        int wynik=0;
        Scanner file = new Scanner(new File(str));
        while (file.hasNext()){
            if(file.next().equals(wyraz))wynik++;
        }
        return wynik;
    }
}
