package pl.edu.uwm.wmii.tadeuszbienkowski.laboratorium03;
import java.util.Scanner;

public class Zadanie1GNice {

    public static void main(String[] args) {
        Scanner in = new Scanner(System.in);
        System.out.print("Podaj liczbe: ");
        String liczba = in.next();
        liczba=nice(liczba);
        System.out.println(liczba);
        System.out.print("Podaj liczbe: ");
        String liczba2 = in.next();
        System.out.print("Podaj co ile separator: ");
        int sep =in.nextInt();
        liczba2=niceSep(liczba2,sep);
        System.out.println(liczba2);
    }

    public static String nice(String str) {
        StringBuffer wynik = new StringBuffer();
        int i = 0;
        if (str.length() % 3 != 0 && str.length() > 3) {
            wynik.append(str.substring(0, str.length() % 3));
            i = str.length() % 3;
        }
        if (str.length() > 3) {
            while (i < str.length()) {
                if(i!=0) wynik.append("'");
                wynik.append(str.substring(i,i+3));
                i += 3;
            }
        } else wynik.append(str);
        return wynik.toString();
    }
    public static String niceSep(String str,int sep) {
        StringBuffer wynik = new StringBuffer();
        int i = 0;
        if (str.length() % sep != 0 && str.length() > sep) {
            wynik.append(str.substring(0, str.length() % sep));
            i = str.length() % sep;
        }
        if (str.length() > sep) {
            while (i < str.length()) {
                if(i!=0) wynik.append("'");
                wynik.append(str.substring(i,i+sep));
                i += sep;
            }
        } else wynik.append(str);
        return wynik.toString();
    }
}
