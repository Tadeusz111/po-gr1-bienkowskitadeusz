package pl.imiajd.Bienkowski;
import java.time.LocalDate;

public abstract class Instrument {
    public Instrument(String pro,int y,int m,int d){
        this.producent=pro;
        this.rokProdukcji=LocalDate.of(y,m,d);

    }
    public String getProducent(){return this.producent;}
    public  LocalDate getRokProdukcji(){return this.rokProdukcji;}
    public abstract void dzwiek();
    public String toString(){
        return ""+this.producent+" "+this.rokProdukcji;
    }
    @Override  public boolean equals(Object otherObject){
        if(this ==otherObject)return true;
        if(otherObject==null)return false;
        if(getClass()!=otherObject.getClass())return false;
        if(!(otherObject instanceof Instrument))return false;
        Instrument other=(Instrument)otherObject;
        if(this.rokProdukcji.getDayOfMonth()==other.getRokProdukcji().getDayOfMonth() &&
                this.rokProdukcji.getYear()==other.getRokProdukcji().getYear() &&
                this.rokProdukcji.getMonth()==other.getRokProdukcji().getMonth() && this.producent==other.getProducent())return true;
        else return false;
    }
    private LocalDate rokProdukcji;
    private String producent;
}
