package pl.edu.uwm.wmii.tadeuszbienkowski.laboratorium04;
import java.util.ArrayList;

public class Zadanie2Marge {
    public static void main(String[] args) {
        ArrayList<Integer> a = new ArrayList<Integer>();
        a.add(2);
        a.add(3);
        a.add(4);
        a.add(5);
        a.add(6);
        System.out.println(a);
        ArrayList<Integer> b = new ArrayList<Integer>();
        b.add(6);
        b.add(7);
        b.add(8);
        b.add(9);
        b.add(0);
        System.out.println(b);
        ArrayList<Integer> c = marge(a, b);
        System.out.println(c);
    }
    public static ArrayList<Integer> marge(ArrayList<Integer> a, ArrayList<Integer> b) {
        ArrayList<Integer> wynik = new ArrayList<Integer>(a.size() + b.size());
        int i = 0;
        while (i < a.size() && i < b.size()) {
            wynik.add(a.get(i));
            wynik.add(b.get(i));
            i++;
        }
        if (a.size() > b.size()) {
            while (i < a.size()) {
                wynik.add(a.get(i));
                i++;
            }
            if (a.size() < b.size()) {
                while (i < b.size()) {
                    wynik.add(a.get(i));
                    i++;
                }
            }
        }
        return wynik;
    }
    public static ArrayList<Integer> margeSort(ArrayList<Integer> a, ArrayList<Integer> b) {
        ArrayList<Integer> wynik = new ArrayList<Integer>(a.size() + b.size());
        int i = 0,x=0,y=0;

        while (i < a.size() +b.size()) {
            wynik.add(a.get(i));
            wynik.add(b.get(i));
            i++;
        }
        return wynik;
    }
}
