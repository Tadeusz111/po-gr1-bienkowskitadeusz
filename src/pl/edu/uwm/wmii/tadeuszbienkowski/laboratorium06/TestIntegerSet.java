package pl.edu.uwm.wmii.tadeuszbienkowski.laboratorium06;
import java.util.Random;

public class TestIntegerSet {
    public static void main(String[]args){
        IntegerSet i1=new IntegerSet();
        IntegerSet i2=new IntegerSet();
        IntegerSet i3=new IntegerSet();
        Random r=new Random();
        for(int i=0;i<30;i++) {
            i1.insertElement(r.nextInt(99)+1);
            i2.insertElement(r.nextInt(99)+1);
        }
        System.out.println("insert 1: "+i1.toString());
        System.out.println("insert 2: "+i2.toString());
        for(int i=0;i<30;i++) {
            i1.insertDelate(r.nextInt(99)+1);
            i2.insertDelate(r.nextInt(99)+1);
        }
        System.out.println("insert 1: "+i1.toString());
        System.out.println("insert 2: "+i2.toString());
        //System.out.println("equals: "+i1.equals(i2));
        i3.setTab(IntegerSet.union(i2.getTab(),i1.getTab()));
        System.out.println("insert 3(union) : "+i3.toString());
        i3.setTab(IntegerSet.intersection(i2.getTab(),i1.getTab()));
        System.out.println("insert 3(intersection) : "+i3.toString());
    }
}
