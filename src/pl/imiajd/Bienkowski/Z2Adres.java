package pl.imiajd.Bienkowski;

public class Z2Adres {
    public Z2Adres(String ulica,int nrD,int nrM,String miasto,String kodP){
        this.ulica=ulica;
        this.kod_pocztowy=kodP;
        this.numer_domu=nrD;
        this.numer_mieszkania=nrM;
        this.miasto=miasto;
    }
    public Z2Adres(String ulica,int nrD,String miasto,String kodP){
        this.ulica=ulica;
        this.kod_pocztowy=kodP;
        this.numer_domu=nrD;
        this.miasto=miasto;
        this.numer_mieszkania=-1;
    }
    public void pokaz(){
        System.out.println(this.miasto+" "+this.kod_pocztowy);
        System.out.println(this.ulica+" "+this.numer_domu+" "+(this.numer_mieszkania<0? "":this.numer_mieszkania));
    }
    public boolean przed(String kodP){
        if(kodP==this.kod_pocztowy) return true;
        else return false;
    }


    private String ulica;
    private int numer_domu,numer_mieszkania;
    private String miasto, kod_pocztowy;
}
