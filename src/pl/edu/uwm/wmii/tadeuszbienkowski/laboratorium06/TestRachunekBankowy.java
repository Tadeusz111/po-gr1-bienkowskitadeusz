package pl.edu.uwm.wmii.tadeuszbienkowski.laboratorium06;

public class TestRachunekBankowy {
    public static void main(String[]args) {
        RachunekBankowy saver1 = new RachunekBankowy(2000.00);
        RachunekBankowy saver2 = new RachunekBankowy(3000.00);
        RachunekBankowy.setRocznaStopaProcentowa(0.04);
        saver1.obliczMiesieczneOdsetki();
        saver2.obliczMiesieczneOdsetki();
        System.out.println("saldo po 1 m dla save1: "+saver1.getsaldo());
        System.out.println("saldo po 1 m dla save2: "+saver2.getsaldo());
    }
}
