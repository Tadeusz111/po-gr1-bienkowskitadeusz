package pl.edu.uwm.wmii.tadeuszbienkowski.laboratorium02;
import java.util.Random;
import java.util.Scanner;

public class IloczynMacierzy {
    public static void main(String []args){
        Scanner in=new Scanner((System.in));
        System.out.print("podaj m n k: ");
        int m=in.nextInt();
        int n=in.nextInt();
        int k=in.nextInt();
        int a[][]=new int [m][n];
        int b[][]=new int [n][k];
        int c[][]=new int [m][k];
        generuj(a,0,9);
        generuj(b,0,9);
        wyswietl(a);
        wyswietl(b);
        iloczyn(a,b,c);
        wyswietl(c);
    }
    public  static  void  generuj(int tab[],int min,int max){
        Random r=new Random();
        for(int i=0;i<tab.length;i++){
            tab[i]=r.nextInt(max-min+1);
        }
    }
    public static void generuj(int tab [][],int min,int max){
        for(int i=0;i<tab.length;i++){
            generuj(tab[i],min,max);
        }
    }
    public static void wyswietl(int tab[]){
        for(int el:tab){
            System.out.print(el+" ");
        }
    }
    public static void wyswietl(int tab[][]){
        for(int []el:tab){
            wyswietl(el);
            System.out.println();
        }
        System.out.println();
    }
    public static void iloczyn(int a[][],int b[][],int c[][]){
        for(int i=0;i<c.length;i++){
            for(int k=0;k<c[i].length;k++){
                for(int j=0;j<b.length;j++){
                    c[i][k]+=a[i][j]*b[j][k];
                }
            }
        }
    }
}
