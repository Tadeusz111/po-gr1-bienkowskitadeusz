package pl.edu.uwm.wmii.tadeuszbienkowski.laboratorium03;
import java.math.BigInteger;
import java.util.Scanner;

public class Zadanie4BigIntZliczZiarenka {
    public static void main(String []args){
        Scanner in = new Scanner(System.in);

        System.out.print("Podaj liczbe n : ");
        int n=in.nextInt();
        BigInteger zarenkaN=ziarenka(n);
        System.out.println(zarenkaN.toString());

    }
    public static BigInteger ziarenka(int n){
        BigInteger wynik=new BigInteger("0");
        BigInteger pole=new BigInteger("1");
        for(int i=0 ;i<n*n;i++){
            wynik=pole.add(wynik);
            pole=pole.add(pole);
        }
        return wynik;
    }
}
