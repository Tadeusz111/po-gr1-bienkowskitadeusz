package pl.edu.uwm.wmii.tadeuszbienkowski.laboratorium07;

import pl.imiajd.Bienkowski.Student;
import pl.imiajd.Bienkowski.Nauczyciel;
import pl.imiajd.Bienkowski.Osoba;

public class Z4_5TestOsobaStudentNauczyciel {
    public static void main(String[]args){
        Osoba o1=new Osoba("Tadusz",1998);
        System.out.println(o1.getNazwisko()+" "+o1.getRokUrozenia());
        System.out.println(o1.toString());

        Student s1=new Student("Janusz",2012,"Plastyka");
        System.out.println(s1.getNazwisko()+" "+s1.getRokUrozenia()+" "+s1.getKierunek());
        System.out.println(s1.toString());

        Nauczyciel n1=new Nauczyciel("Zdzisiek",2020,4000000);
        System.out.println(n1.getNazwisko()+" "+n1.getRokUrozenia()+" "+n1.getPensja());
        System.out.println(n1.toString());

        Osoba o2=new Student("Harry",432,"figlei psikusy");
        System.out.println(o2.getNazwisko()+" "+o2.getRokUrozenia()+" "+((Student) o2).getKierunek());
        System.out.println(o2.toString());

        Osoba o3=new Nauczyciel("Mareczek",2000,341);
        System.out.println(o3.getNazwisko()+" "+o3.getRokUrozenia()+" "+(((Nauczyciel) o3).getPensja()));
        System.out.println(o3.toString());
    }
}
