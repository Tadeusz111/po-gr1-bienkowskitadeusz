package pl.edu.uwm.wmii.tadeuszbienkowski.laboratorium03;
import java.util.Scanner;
import java.math.BigDecimal;
import java.math.RoundingMode;

public class Zadanie5Lokata {
    public static void main(String []args){
        Scanner in=new Scanner(System.in);
        System.out.print("podaj kwote poczatkowo: ");
        float k=in.nextFloat();
        System.out.print("podaj sope oprocentowania : ");
        float p;
        p=in.nextFloat();
        System.out.print("podaj dlugosc lokaty w latach: ");
        int n=in.nextInt();
        BigDecimal PoKincuLokaty=zliczlokate(k,p,n);
        System.out.print(PoKincuLokaty.toString());
    }
    public static BigDecimal zliczlokate(float k,float p,int n){
        BigDecimal pom=new BigDecimal(p);
        BigDecimal wynik=new BigDecimal(k);
        for(int i=0;i<n;i++){
            wynik=wynik.add(wynik.multiply(pom));
        }
        wynik= wynik.setScale(2,RoundingMode.HALF_UP);
        return  wynik;
    }
}
