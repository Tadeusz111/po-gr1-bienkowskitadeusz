package pl.edu.uwm.wmii.tadeuszbienkowski.laboratorium08;
import pl.imiajd.Bienkowski.Osoba;
import pl.imiajd.Bienkowski.Student;
import pl.imiajd.Bienkowski.Pracownik;

public class Zadanie1_2TestOsoba {
    public static void main(String[] args)
    {
        Osoba[] ludzie = new Osoba[2];

        ludzie[0] = new Pracownik(" Nowak",new String[]{"Kamil"},true, 1990,11,11,5000, 1999,12,12);
        ludzie[1] = new Student(" Borabora", new String[]{"Malgorzata"},false,1899,1,1,"informatyka",7.64);
        //ludzie[2] = new Osoba("Kranic Karol");

        for (Osoba p : ludzie) {
            System.out.println(p.getNazwisko() + ": " + p.getOpis());
        }
    }
}
