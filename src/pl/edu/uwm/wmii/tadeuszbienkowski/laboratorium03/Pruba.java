package pl.edu.uwm.wmii.tadeuszbienkowski.laboratorium03;
import java.util.Random;
import java.util.Scanner;

public class Pruba {
    public static void main(String[]arbs){
        Scanner in=new Scanner(System.in);
        System.out.print("podaj n (1<=n<=150); ");
        int n;
        do {
            n= in.nextInt();
            if(1>n ||n>150)  System.out.print("n nie wlasciwe \n podaj n (1<=n<=150); ");
        }while (1>n || n >150);
        double tab[]=new double[n];
        System.out.print("podaj n licz rzecywistych ; ");
        for(int i=0;i<n;i++){
            tab[i]=in.nextDouble();
        }
        double min=tab[0],max=tab[0];
        for(double el:tab){
            if(max<el){
                max=el;
            }else if (min>el){
                min=el;
            }
        }
        System.out.println("max = "+max+" min = "+min);
        int tab2[]=new int[n];
        generuj(tab2,-777,150);
        wyswietl(tab2);
        System.out.println("suma dodatnih = "+sumD(tab2)+" suma ujemnych = "+sumU(tab2));


    }
    public static void generuj(int tab[],int min,int max){
        Random r =new Random();
        for (int i=0;i<tab.length;i++){
            tab[i]=r.nextInt(max-min+1)-min;
        }
    }
    public static void wyswietl(int[] tab){
        for(int el:tab){
            System.out.print(el+" ");
        }
        System.out.println();
    }
    public static int sumD(int []tab){
        int wynik=0;
        for(int el:tab){
            if(el>0) wynik+=el;
        }
        return wynik;
    }
    public static int sumU(int []tab){
        int wynik=0;
        for(int el:tab){
            if(el<0) wynik+=el;
        }
        return wynik;
    }
}
