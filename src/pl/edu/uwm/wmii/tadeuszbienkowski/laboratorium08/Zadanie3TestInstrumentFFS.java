package pl.edu.uwm.wmii.tadeuszbienkowski.laboratorium08;
import pl.imiajd.Bienkowski.Flet;
import pl.imiajd.Bienkowski.Fortepian;
import pl.imiajd.Bienkowski.Skrzypce;
import pl.imiajd.Bienkowski.Instrument;

import java.util.ArrayList;

public class Zadanie3TestInstrumentFFS {
    public static void main(String[]args){
        ArrayList<Instrument> ins=new ArrayList<Instrument>(5);
        ins.add(new Flet("a",1800,11,12));ins.add(new Fortepian("B",1700,1,2));
        ins.add(new Skrzypce("sttiwariius",1700,2,12));
        ins.add(new Fortepian("XXX",1600,11,11)); ins.add(new Skrzypce("sttiwariius",1700,2,12));
        for(Instrument el:ins){
            System.out.println(el.toString());
            el.dzwiek();
        }
        for(int i=0;i<4;i++){
            for(int k=i+1;k<5;k++){
                System.out.println(ins.get(i)+" equals "+ins.get(k)+" ="+ins.get(i).equals(ins.get(k)));
            }

        }

    }

}
