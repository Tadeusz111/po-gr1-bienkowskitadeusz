package pl.edu.uwm.wmii.tadeuszbienkowski.laboratorium03;
import java.io.File;
import java.io.FileNotFoundException;
import java.util.Scanner;

public class Zadanie2ZliczZPlikuChar {
    public static void main(String []args)throws FileNotFoundException
    {
        Scanner in =new Scanner(System.in);
        System.out.print("podaj sciezkie pliku: ");

        String plik=in.nextLine();
        Scanner file=new Scanner(new File(plik));
        String str=new String();
        while(file.hasNextLine())
        {
            str+=file.nextLine();
        }
        System.out.print("podaj szukany char: ");
        String c=in.next();
        for(int i=0;i<c.length();i++)
        {
            System.out.println(c.charAt(i)+" = "+ileChar(str,c.charAt(i)));
        }
    }
    public static int ileChar(String str,char c){
        int wynik=0;
        for(int i=0;i<str.length();i++){
            if(str.charAt(i)==c) wynik++;
        }
        return wynik;
    }
}
