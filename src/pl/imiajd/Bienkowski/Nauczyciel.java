package pl.imiajd.Bienkowski;

public class Nauczyciel extends Osoba {
    public Nauczyciel(String n,int r,int pensja){
        super( n, r);
        this.pensja=pensja;
    }
    public int getPensja(){
        return this.pensja;
    }
    public String toString(){
        String wynik=super.toString();
        return wynik+=" "+this.pensja;
    }
    private int pensja;
}
