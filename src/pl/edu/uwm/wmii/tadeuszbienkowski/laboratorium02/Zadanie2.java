package pl.edu.uwm.wmii.tadeuszbienkowski.laboratorium02;
import java.util.Random;
import java.util.Scanner;


public class Zadanie2 {
    public static void main(String[] args) {
        Scanner in = new Scanner(System.in);
        System.out.print("podaj 1<=n<=100: ");
        int n = in.nextInt();
        int[] tab = new int[n];
        generuj(tab, n, -999, 999);
        wypisz(tab);
        System.out.println("ile nie parzustych "+ileNieparzystych(tab));
        System.out.println("ile  parzustych "+ileParzystych(tab));
        System.out.println("ile dodatnich "+ileDodatnich(tab));
        System.out.println("ile ujemnych "+ileUjemnych(tab));
        System.out.println("ile zer: "+ileZer(tab));
        System.out.println("ile maksymalnych "+ileMaksymalnych(tab));
        System.out.println("suma dodatnich "+sumaDodatnich(tab));
        System.out.println("suma ujemnych "+sumaUjemnych(tab));
        System.out.println("dlugosc maksymalnego ciagu dodatniego "+dlugoscMaxCiagDodatni(tab));
        System.out.print("podaj lewy ,prawy1<l/p<n: ");
        int prawy=in.nextInt();
        int lewy=in.nextInt();
        odwracanieFragmentem(tab,lewy,prawy);
        signum(tab);
    }

    public static void generuj(int[] tab, int n, int min, int max) {
        Random r = new Random();

        for (int i = 0; i < n; i++) {

            tab[i] = r.nextInt(((max - min))) + (min);
        }
    }

    /////////////////////////////////////////////a/////////////////////////////////////////////////////
    public static int ileNieparzystych(int tab[]) {
        int zNp = 0;
        for (int a : tab) {
            if (a % 2 != 0) zNp++;
        }
        return zNp;
    }

    public static int ileParzystych(int tab[]) {
        int zp = 0;
        for (int a : tab) {
            if (a % 2 == 0) zp++;
        }
        return zp;
    }

    /////////////////////////////////////////////////b////////////////////////////////////////////////////////////
    public static int ileDodatnich(int tab[]) {
        int wynik = 0;
        for (int a : tab) {
            if (a > 0) wynik++;
        }
        return wynik;
    }

    public static int ileUjemnych(int tab[]) {
        int wynik = 0;
        for (int a : tab) {
            if (a < 0) wynik++;
        }
        return wynik;
    }

    public static int ileZer(int tab[]) {
        int wynik = 0;
        for (int a : tab) {
            if (a == 0) wynik++;
        }
        return wynik;
    }

    ///////////////////////////////////////////////////c//////////////////////////////////////////////////////////
    public static int ileMaksymalnych(int tab[]) {
        int wynik = 0;
        int max = tab[0];
        for (int i = 1; i < tab.length; i++) {
            if (max < tab[i]) max = tab[i];
        }
        for (int a : tab) {
            if (a == max) wynik++;
        }
        return wynik;

    }

    ///////////////////////////////////////////////////////d////////////////////////////////////////////////////////
    public static int sumaDodatnich(int tab[]) {
        int wynik = 0;
        for (int a : tab) {
            if (a > 0) wynik += a;
        }
        return wynik;
    }

    public static int sumaUjemnych(int tab[]) {
        int wynik = 0;
        for (int a : tab) {
            if (a < 0) wynik += a;
        }
        return wynik;
    }

    /////////////////////////////////////////////////////////e////////////////////////////////////////////////////////
    public static int dlugoscMaxCiagDodatni(int tab[]) {
        int wynik = 0;
        int max = tab[0];
        for (int i = 1; i < tab.length; i++) {
            if (max < tab[i]) max = tab[i];
        }
        if (max > 0) {
            for (int k = 1; max % k != max; k *= 10, ++wynik) ;
        }
        return wynik;
    }

    //////////////////////////////////////////////f////////////////////////////////////////////////////////////////
    public static void signum(int tab[]) {

        for (int i = 0; i < tab.length; i++) {
            if (tab[i] < 0) tab[i] = -1;
            if (tab[i]> 0) tab[i] = 1;
        }
        wypisz(tab);
    }

    //////////////////////////////////////////////////g//////////////////////////////////////////////////////
    public static void odwracanieFragmentem(int tab[], int lewy, int prawy) {
        if (lewy < prawy) {
            for (; lewy < prawy; lewy++, prawy--) {
                int buf = tab[lewy];
                tab[lewy] = tab[prawy];
                tab[prawy] = buf;
            }
        } else {
            for (; lewy > prawy; lewy--, prawy++) {
                int buf = tab[lewy];
                tab[lewy] = tab[prawy];
                tab[prawy] = buf;
            }
        }
        wypisz(tab);
    }
    public static void wypisz ( int[] tab){
        for (int el : tab) {
            System.out.print(el + " ");
        }
        System.out.println("");
    }
}