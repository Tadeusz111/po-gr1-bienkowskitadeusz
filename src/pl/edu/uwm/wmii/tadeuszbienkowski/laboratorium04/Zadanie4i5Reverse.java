package pl.edu.uwm.wmii.tadeuszbienkowski.laboratorium04;

import javax.lang.model.type.NullType;
import java.util.ArrayList;

public class Zadanie4i5Reverse {
    public static void main(String[]args){
        ArrayList<Integer> a = new ArrayList<Integer>();
        a.add(2);
        a.add(3);
        a.add(4);
        a.add(5);
        a.add(6);  a.add(8);  a.add(9); // a.add(0);
        System.out.println(a);
        ArrayList<Integer> c = reverse(a);
        System.out.println(c);
        reverseB(a);
        System.out.println(a);

    }
    public static  ArrayList<Integer> reverse( ArrayList<Integer>a){
        ArrayList<Integer>wynik=new  ArrayList<Integer>(a.size());
        for(int i=a.size()-1;i>=0;i--){
            wynik.add(a.get(i));
        }

        return wynik;
    }
    public static  void reverseB( ArrayList<Integer>a){
        ArrayList<Integer>wynik=new  ArrayList<Integer>(a.size());

        Integer buf;
        int k=a.size()-1;
        for(int i=k;i>=(int)a.size()/2;i--){
            buf=a.get(i);
            a.set(i,a.get(k-i));
            a.set(k-i,buf);

        }

    }
}
