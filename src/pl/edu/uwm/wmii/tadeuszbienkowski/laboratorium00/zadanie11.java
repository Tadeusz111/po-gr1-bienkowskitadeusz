package pl.edu.uwm.wmii.tadeuszbienkowski.laboratorium00;

public class zadanie11 {
    public static void main(String[] args){
        System.out.println("Album\n" +
                "\n" +
                "Nikt w rodzinie nie umarł z miłości.\n" +
                "Co tam było to było, ale nic dla mitu.\n" +
                "Romeowie gruźlicy? Julie dyfrerytu?\n" +
                "Niektórzy wręcz dożyli zgrzybiałej starości.\n" +
                "Żadnej ofiary braku odpowiedzi\n" +
                "na list pokropiony łzami!\n" +
                "Zawsze w końcu zjawiali się sąsiedzi\n" +
                "z różami i binokularami.\n" +
                "Żadnego zaduszenia w stylowej szafie,\n" +
                "kiedy to raptem wraca mąż kochanki!\n" +
                "Nikomu te sznurówki, mantylki firanki, falbanki\n" +
                "nie przeszkodziły wejść na fotografię.\n" +
                "I nigdy w duszy piekielnego Boscha!\n" +
                "I nigdy z pistoletem do ogrodu!\n" +
                "(Konali z kulą w czaszce, ale z innego powodu\n" +
                "i na polowych noszach)\n" +
                "Nawet ta, z ekstatycznym kokiem\n" +
                "i oczami podkutymi jak po balu,\n" +
                "odpłynęła wielkim krwotokiem\n" +
                "nie do ciebie, danserze, i nie z żalu.\n" +
                "Może ktoś, dawniej, przed dagerotypem -\n" +
                "ale z tych, co w albumie, nikt, o ile wiem.\n" +
                "Rozśmieszały się smutki, leciał dzień za dniem,\n" +
                "a oni, pocieszeni, znikali na grypę.\n");


    }
}