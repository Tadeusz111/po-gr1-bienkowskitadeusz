package pl.edu.uwm.wmii.tadeuszbienkowski.laboratorium03;
import java.util.Scanner;
import java.lang.Object;

public class Zadanie1FChange {
    public static void main(String[] args) {
        Scanner in = new Scanner(System.in);
        System.out.print("podaj string: ");
        String str=new String();
        str= in.nextLine();
        str=change(str);
        System.out.println(str);
    }
    public static String change(String str){
        StringBuffer wynik=new StringBuffer();
        for(int i=0;i<str.length();i++){
            if(str.charAt(i)>='a' && str.charAt(i)<='z' ){
                wynik.append ( str.substring(i,i+1).toUpperCase());
            }else if(str.charAt(i)>='A' && str.charAt(i)<='Z' ){
                wynik.append ( str.substring(i,i+1).toLowerCase());
            }else  wynik.append ( str.substring(i,i+1));
        }
        return wynik.toString();
    }
}
