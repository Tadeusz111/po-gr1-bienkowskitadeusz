package pl.imiajd.Bienkowski;
import java.time.LocalDate;

public class Pracownik extends Osoba
{
    public Pracownik(String nazwisko,String[]imona,boolean plec,int y,int m,int d, double pobory,int yz,int mz,int dz) {
        super( nazwisko,imona, plec,y, m,d);
        this.pobory = pobory;
        this.dataZatrudnienia=LocalDate.of(yz,mz,dz);
    }

    public LocalDate getDataZatrudnienia(){return dataZatrudnienia;}
    public double getPobory() {
        return pobory;
    }

    public String getOpis() {
        return String.format("pracownik z pensją %.2f zł", pobory);
    }
    private double pobory;
    private LocalDate dataZatrudnienia;
}


