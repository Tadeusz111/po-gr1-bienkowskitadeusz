package pl.edu.uwm.wmii.tadeuszbienkowski.laboratorium02;
import java.util.Random;
import  java.util.Scanner;

public class Zadanie1 {
    public static void main (String [] args){
        Scanner in=new Scanner(System.in);
        System.out.print("podaj 1<=n<=100: ");
        int n =in.nextInt();
        System.out.print("podaj lewy ,prawy1<l/p<n: ");
        int prawy=in.nextInt();
        int lewy=in.nextInt();
        int [] tab=new int[n];
        generuj(tab,1000);
        wypisz(tab);
        int zP=0,zNP=0;
        int d=0,u=0,z=0;
        int max=tab[0],min=tab[0],smin=0,smax=0;
        int su=0,sd=0;
        int dd=0,du=0;
        for(int i=0;i<n;i++){
            //a
            if(tab[i]%2==0)zP++;
            else zNP++;
            //b
            if(tab[i]>0)d++;
            else if(tab[i]<0)u++;
            else z++;
            //c
            if(i==0) {
                for (int k = 1; k < n; k++) {
                    if (max < tab[k]) max=tab[k];
                    if (min > tab[k]) min=tab[k];
                }
            }
            if(tab[i]==max)  smax++;
            if(tab[i]==min)  smin++;
            //d
            if(tab[i]>0)sd+=tab[i];
            if(tab[i]<0)su+=tab[i];
            //e
            if(i==0) {
                if (max > 0) {
                    for (int k = 1; max % k != max; k *= 10, ++dd) ;
                }
                if (min < 0) {
                    for (int k = 1; min % k != min; k *= 10, ++du) ;
                }
            }
            if(lewy<prawy){
                for(;lewy<prawy;lewy++,prawy--){
                    int buf=tab[lewy];
                    tab[lewy]=tab[prawy];
                    tab[prawy]=buf;
                }
            }else {
                for(;lewy>prawy;lewy--,prawy++){
                    int buf=tab[lewy];
                    tab[lewy]=tab[prawy];
                    tab[prawy]=buf;
                }
            }
        }
        System.out.println("a)parzyste "+zP+"\n Nie parzyste "+zNP);
        System.out.println("b)dodatnie "+d+"\n ujemne "+zNP+"\n zera "+z);
        System.out.println("c)max "+max+"\n min "+min+"\n suma max "+smax+"\n suma min "+smin);
        System.out.println("d)suma dodatnie "+sd+"\n suma ujemne "+su);
        System.out.println("e)dlugosc max dodatnie "+dd+"\n dlugossc min ujemne "+du);
        wypisz(tab);
        for(int i=0;i<n;i++){
            if(tab[i]<0)tab[i]=-1;
            if(tab[i]>0)tab[i]=1;
        }
        wypisz(tab);

    }
    public static void generuj(int [] tab,int max){
        Random r=new Random();
        for (int i=0;i<tab.length;i++){
            tab[i]=r.nextInt(((2*max)-2))-(max-1);
        }
    }
    public static void wypisz(int []tab){
        for(int el:tab){
            System.out.print(el+ " ");
        }
        System.out.println("");
    }
}
