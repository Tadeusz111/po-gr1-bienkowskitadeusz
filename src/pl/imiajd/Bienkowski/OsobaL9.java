package pl.imiajd.Bienkowski;

import java.time.LocalDate;

public class OsobaL9 implements Comparable<OsobaL9> {
    public OsobaL9(String n ,int y,int m,int d){
        this.nazwisko=n;
        this.dataUrodenia=LocalDate.of(y,m,d);
    }
    @Override
    public int compareTo(OsobaL9 o) {
        if(getNazwisko().compareTo(o.getNazwisko())==0) {
            if (getDataUrodenia().compareTo(o.getDataUrodenia()) == 0) {
                return 0;
            } else if (getDataUrodenia().compareTo(o.getDataUrodenia()) >= 1) return 1;
            else return -1;
        }
           /*
            if (getDataUrodenia().getYear() == o.getDataUrodenia().getYear()) {
                if (getDataUrodenia().getMonth() == o.getDataUrodenia().getMonth()) {
                    if (getDataUrodenia().getDayOfMonth() == o.getDataUrodenia().getDayOfMonth()) {
                        return 0;
                    } else if (getDataUrodenia().getDayOfMonth() > o.getDataUrodenia().getDayOfMonth()) return -1;
                    else return 1;
                } else if (getDataUrodenia().getMonth().compareTo(o.getDataUrodenia().getMonth())==1) return -1;
                else return 1;
            } else if (getDataUrodenia().getYear() > o.getDataUrodenia().getYear()) return -1;
            else return 1;
        }
        */
        else if (getNazwisko().compareTo(o.getNazwisko())<=1)return 1;
        else   return -1;
    }
    public String toString(){
        return "["+getNazwisko()+"] "+this.dataUrodenia.getYear()+"-"+this.dataUrodenia.getMonth()+"-"+this.dataUrodenia.getDayOfMonth();
    }
    @Override public boolean equals(Object otherObject){
        if (this==otherObject)return true;
        if (otherObject==null)return false;
        if(getClass()!=otherObject.getClass())return false;
        if(!(otherObject instanceof OsobaL9))return false;
        OsobaL9 other=(OsobaL9)otherObject;
        if(getNazwisko()==other.getNazwisko()){
            if(getDataUrodenia().getYear()==other.getDataUrodenia().getYear())
                if(getDataUrodenia().getMonth()==other.getDataUrodenia().getMonth())
                    if(getDataUrodenia().getDayOfMonth()==other.getDataUrodenia().getDayOfMonth()) return true;
        }
        return false;

    }
    public String getNazwisko() {
        return nazwisko;
    }
    public LocalDate getDataUrodenia() {
        return this.dataUrodenia;
    }
    public int ileLat(){
        return LocalDate.now().getYear()-getDataUrodenia().getYear();
    }
    public int ileMiesiecy(){
        int wynik=0;
        if(LocalDate.now().compareTo(getDataUrodenia())<=1){
            wynik+=12-getDataUrodenia().getMonthValue()+LocalDate.now().getMonthValue();
        }
        else{
            wynik+=LocalDate.now().getMonthValue()-getDataUrodenia().getMonthValue();
        }
        return Math.abs(wynik);
    }
    public int ileDni(){
        int wynik=0,buf=0;
        int m=getDataUrodenia().getMonthValue();

        if(m==1 ||m==1||m== 3||m== 5||m==7||m==8||m==10||m==12){
            wynik+=31;
            buf=31;
        }else if(m==4||m==6||m==9||m==11){
            wynik+=30;
            buf=30;
        }
        else{
            if(LocalDate.now().getYear()%4==0 &&LocalDate.now().getYear()%100!=0)
            {   wynik+=29;
                buf=29;
            }
            else {
                wynik += 28;
                buf=28;
            }
        }

        return Math.abs( wynik-getDataUrodenia().getDayOfMonth()-(buf-LocalDate.now().getDayOfMonth()));
    }
    private String nazwisko;
    private LocalDate dataUrodenia;
}
