package pl.edu.uwm.wmii.tadeuszbienkowski.laboratorium03;

public class Zadanie1AiB {
    public static void main(String []args){
        String str="abczababsxcz";

        System.out.println("napi 1:"+str+"\n ilosc a="+ countChar(str,'a'));
        System.out.println("powturzenia ="+"ab"+ countSubStr(str,"ab"));
    }
    public static int countChar(String str,char c){
        int wynik=0;
        for (int i=0;i< str.length();i++){
            if(str.charAt(i)==c)wynik++;
        }
        return wynik;
    }
    public static int countSubStr(String str ,String sub){
        int wynik=0;
        for(int i=0;i<str.length()-sub.length();i++){
            if((str.substring(i,i+sub.length())).equals(sub))wynik++;
        }
        return wynik;
    }
}
