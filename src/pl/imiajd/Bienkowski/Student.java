package pl.imiajd.Bienkowski;

public class Student extends Osoba {
    public Student(String n,int r,String kier){
        super( n,r);
        this.kierunek=kier;
    }
    //kon lab8 zad1
    public Student(String nazwisko,String[]imona,boolean plec,int y,int m,int d,String kier,double sredniaOcenrO){
        super( nazwisko,imona, plec, y, m, d);
        this.kierunek=kier;
        this.sredniaOcen=sredniaOcenrO;
    }
    public String getOpis()
    {
        return "kierunek studiów: " + kierunek;
    }

    public double getSredniaOcen() {
        return this.sredniaOcen;
    }
    public void setSredniaOsne(double sredniaOcen){
        this.sredniaOcen=sredniaOcen;
    }
    public String getKierunek(){
        return this.kierunek;
    }
    public String toString(){
        String wynik=super.toString();
        return wynik+=" "+this.kierunek;
    }
    private String kierunek;
    private double sredniaOcen;
}
