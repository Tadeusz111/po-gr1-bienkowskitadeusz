package pl.edu.uwm.wmii.tadeuszbienkowski.laboratorium06;

public class IntegerSet {

    public IntegerSet(){
        this.tab=new boolean[100];
    }
    static boolean[] union(boolean[]t1,boolean[]t2){
        boolean []wynik=new boolean[t1.length> t2.length? t1.length:t2.length];
        for(int i=0;i<wynik.length;i++){
            if(t1[i] ||t2[i]) wynik[i]=true;
        }
        return wynik;
    }
    static boolean[] intersection(boolean[]t1,boolean[]t2){
        boolean []wynik=new boolean[t1.length> t2.length? t1.length:t2.length];
        for(int i=0;i<wynik.length;i++){
            if(t1[i] &&t2[i]) wynik[i]=true;
        }
        return wynik;
    }
    public void insertElement(int a){
        if(a>0&&a<=100) this.tab[a-1]=true;
    }
    public void insertDelate(int a){
        if(a>0&&a<=100) this.tab[a-1]=false;
    }
    public String toString(){
        String wynik=new String();
        for(int i=0;i<tab.length;i++){
            if (tab[i]==true) {
                wynik += i+1 + " ";
            }
        }
        return wynik;
    }
    public boolean equals(Boolean[]t){
        if(tab.length!=t.length) return false;
        else{
            for(int i=0;i<t.length;i++){
                if(tab[i]!=t[i])return false;
            }
        }
        return true;
    }
    public boolean[]getTab(){
        return this.tab;
    }public void setTab(boolean[]t){
        this.tab=t;
    }
    private boolean[]tab;

}
