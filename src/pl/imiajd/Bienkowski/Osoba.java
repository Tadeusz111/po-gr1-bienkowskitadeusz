package pl.imiajd.Bienkowski;
import java.time.LocalDate;

public class Osoba {
    public Osoba(String n,int r){
        this.nazwisko=n;
        this.rokUrodzenia=r;
    }
    //kon lab8 z1 ;
    public String getOpis(){
        return (""+nazwisko+" "+iomina);
    }
    public Osoba(String nazwisko,String[]imona,boolean plec,int y,int m,int d){
        this.nazwisko=nazwisko;
        this.iomina=new String[imona.length];
        this.iomina=imona.clone();
        this.plec=plec;
        this.dataUrodzenia=LocalDate.of(y,m,d);
    }
    public String[] getIomina(){ return this.iomina.clone(); }
    public  boolean getPlec(){return this.plec; }
    public LocalDate getDataUrodzenia(){return dataUrodzenia;}
    //metody lab7
    public String getNazwisko(){
        return this.nazwisko;
    }
    public int getRokUrozenia(){
        return this.rokUrodzenia;
    }
    public String toString(){
        String wynik=new String();
        wynik=""+this.nazwisko+" "+this.rokUrodzenia;
        return wynik;
    }

    private String nazwisko;
    private int rokUrodzenia;
    //lab8
    private String iomina[];
    private LocalDate dataUrodzenia;
    private boolean plec;
}
