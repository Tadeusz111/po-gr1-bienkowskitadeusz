package pl.edu.uwm.wmii.tadeuszbienkowski.laboratorium04;
import java.util.ArrayList;

public class Zadanie1Append {
    public static void main(String[] args) {
        ArrayList<Integer> a = new ArrayList<Integer>();
        a.add(2);
        a.add(3);
        a.add(4);
        a.add(5);
        a.add(6);
        System.out.println(a);
        ArrayList<Integer> b = new ArrayList<Integer>();
        b.add(6);
        b.add(7);
        b.add(8);
        b.add(9);
        b.add(0);
        System.out.println(b);
        ArrayList<Integer> c = append(a, b);
        System.out.println(c);
    }

    public static ArrayList<Integer> append(ArrayList<Integer> a, ArrayList<Integer> b) {
        ArrayList<Integer> wynik = new ArrayList<Integer>(a.size() + b.size());
        wynik.addAll(0, b);
        wynik.addAll(0, a);
        return wynik;
    }
}

