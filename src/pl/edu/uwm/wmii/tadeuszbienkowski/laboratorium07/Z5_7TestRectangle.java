package pl.edu.uwm.wmii.tadeuszbienkowski.laboratorium07;
import pl.imiajd.Bienkowski.BetterRectangle;

public class Z5_7TestRectangle {
    public static void main(String[]args){
        BetterRectangle k=new BetterRectangle(4,4);
        System.out.println(k.toString()+"O="+k.getPerimeter()+"   P="+k.getArea());

        BetterRectangle p=new BetterRectangle(2,4);
        System.out.println(p.toString()+"O="+p.getPerimeter()+"   P="+p.getArea());
    }
}
